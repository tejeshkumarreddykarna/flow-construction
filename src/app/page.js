"use client"
import React from 'react'
import Image from 'next/image'
import { Panellayout } from '../../components/panellayout/Panellayout'
import styles from './page.module.scss'
import Homes from '../../public/icons/house-chimney (1).svg'
import Building from '../../public/icons/city.svg'
import Human from '../../public/icons/baby (1).svg'
import Security from '../../public/icons/life-ring.svg'
import Play from '../../public/icons/play.svg'
import Quote from '../../public/icons/quote-right.svg'
import Services from '../../components/Services/Services'
import { Achievments } from '../../components/Achievements/Achievments'
import Link from 'next/link'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'swiper/scss';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper/modules';
export default function Home() {
    const products = [
        {
            heading: "Services We Provide",
            data: [
                {
                    image: "/images/house1.jpg",
                    title: "Residential Design",
                    description: "  iam a paragraph. Click here to add your own text and edit me.its easy just click Edit Text or double click me",
                    button: "Let's talk"
                },
                {
                    image: "/images/house2.jpg",
                    title: "Workspace Design",
                    description: "Add your own content and make chanes to the font. Feel free to drag me anywhere you like on your page",
                    button: "Let's talk"
                },
                {
                    image: "/images/house3.jpg",
                    title: "Interior Design",
                    description: "Drop me anywhere you like on your page. i'm a great place for you to tell a story and let your users know a title about you",
                    button: "Let's talk"
                },
              
               

            ]
        },
    ]
    const title = [
        {
            small_text:'Our Testimonials',
            achievetitle: "What Our Valuable Clients Say",
            Datas: [
                {
                    description: "Lorem ipsum dolar sit amet, consctet adipiscing elit. Viverra accumsan lobortis looreet proin non in ultrices tortor non.Risus enim quam. Lorem ipsum dolar sit amet, consectetur adipiscing elit. Viverra accumsan lobortis loreet",
                    image: "/images/image13.png",
                    name: "Hannam Odeth",
                    lighttext: "Selling Agent",
                    icon: <Quote />
                },

                {
                    description: "Lorem ipsum dolar sit amet, consctet adipiscing elit. Viverra accumsan lobortis looreet proin non in ultrices tortor non.Risus enim quam. Lorem ipsum dolar sit amet, consectetur adipiscing elit. Viverra accumsan lobortis loreet",
                    image: "/images/image14.png",
                    name: "Hannam Odeth",
                    lighttext: "Selling Agent",
                    icon: <Quote />
                },

                {
                    description: "Lorem ipsum dolar sit amet, consctet adipiscing elit. Viverra accumsan lobortis looreet proin non in ultrices tortor non.Risus enim quam. Lorem ipsum dolar sit amet, consectetur adipiscing elit. Viverra accumsan lobortis loreet",
                    image: "/images/image17.png",
                    name: "Hannam Odeth",
                    lighttext: "Selling Agent",
                    icon: <Quote />
                },
                {
                    description: "Lorem ipsum dolar sit amet, consctet adipiscing elit. Viverra accumsan lobortis looreet proin non in ultrices tortor non.Risus enim quam. Lorem ipsum dolar sit amet, consectetur adipiscing elit. Viverra accumsan lobortis loreet",
                    image: "/images/image17.png",
                    name: "Hannam Odeth",
                    lighttext: "Selling Agent",
                    icon: <Quote />
                },


            ]
        }
    ]
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        arrows: false,
        cssEase: "linear",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    
      };
      const brands = {
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        arrows: false,
        cssEase: "linear",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    
      };
    return (
        <>

            <Panellayout>
                <div className={styles.Statistics_sections}>
                    <div className='contain'>
                        <div className={styles.Statistics_section}>
                            <div className={styles.Statistics_section_blocks}>
                                <div className={styles.Statistics_section_block_1}>
                                    <div className={styles.img_block}>
                                        <Image src='/images/paint-roller.png' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.text_block}>
                                        <h2 className={styles.text}>4k+</h2>
                                        <p className={styles.para}>Service Provided</p>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.Statistics_section_blocks}>
                                <div className={styles.Statistics_section_block_1}>
                                    <div className={styles.img_block}>
                                        <Image src='/images/customer-review.png' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.text_block}>
                                        <h2 className={styles.text}>3.9k+</h2>
                                        <p className={styles.para}>Happy Customers</p>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.Statistics_section_blocks}>
                                <div className={styles.Statistics_section_block_1}>
                                    <div className={styles.img_block}>
                                        <Image src='/images/employee.png' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.text_block}>
                                        <h2 className={styles.text}>550+</h2>
                                        <p className={styles.para}>Workers Employee</p>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.Statistics_section_blocks}>
                                <div className={styles.Statistics_section_block_1}>
                                    <div className={styles.img_block}>
                                        <Image src='/images/time.png' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.text_block}>
                                        <h2 className={styles.text}>7+</h2>
                                        <p className={styles.para}>Years Experience</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.about_us_section}>
                    <div className='contain'>
                        <div className={styles.main_about_us_section}>
                            <div className={styles.left_section}>
                                <div className={styles.left_img_block}>
                                    <div className={styles.img_block}>
                                        <Image src='/images/Red-House.jpg' height="500" width="500" />
                                        <div className={styles.house_design_block}>
                                            <Image src="/images/hall.jpg" height="1000" width="1000" />
                                            <div>
                                                <button><Play /></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className={styles.right_section}>
                                <div className={styles.title_section}>
                                    <p className={styles.title_block}><Link href='/about-us' className={styles.link_block}>About Us</Link></p>
                                </div>
                                <div className={styles.about_us_section_text}>
                                    <h1>The Leading Real Estate Rentan Marketplace</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sadales
                                        amet ut posuere et. Risus pretium vel adiquam elementum.
                                    </p>
                                </div>
                                <div className={styles.main_building_block}>
                                    <div className={styles.desing_block}>
                                        <div className={styles.image_block}>
                                            <Homes />
                                        </div>
                                        <div className={styles.para}>
                                            <p>Smart Home Designe</p>
                                        </div>
                                    </div>
                                    <div className={styles.desing_block}>
                                        <div className={styles.image_block}>
                                            <Building />
                                        </div>
                                        <div className={styles.para}>
                                            <p>Beautiful Scene Around</p>
                                        </div>
                                    </div>
                                    <div className={styles.desing_block}>
                                        <div className={styles.image_block}>
                                            <Human />
                                        </div>
                                        <div className={styles.para}>
                                            <p>Exceptional Lifestyle</p>
                                        </div>
                                    </div>
                                    <div className={styles.desing_block}>
                                        <div className={styles.image_block}>
                                            <Security />
                                        </div>
                                        <div className={styles.para}>
                                            <p>Complete 24/7 cecurity</p>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.btns_block}>
                                    <div className={styles.btn_started}>
                                        <button>Get Started</button>
                                    </div>
                                    <div className={styles.btn_contact}>
                                        <button><Link href='/contact-us' className={styles.link_block}>Contact Us</Link></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.main_service_container}>
                    <div className='contain'>
                        <div className={styles.service_section}>
                            {products.map((text) => {
                                return (
                                    <div>

                                        {
                                            <div className={styles.main_service}>
                                                <h1>{text.heading}</h1>

                                                <div className={styles.service_section}>
                                                    {text.data?.map((value) => {
                                                        return (

                                                            <Services {...{ value }} />
                                                        )
                                                    }
                                                    )}
                                                </div>
                                            </div>
                                        }
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
                <div>
                    <div className='contain'>
                        <div className={styles.main_Gallery_section}>
                            <p className={styles.text}>Photo Gallery</p>
                            <h1 className={styles.gallery_title}>Let's See Our Photo Gallery.</h1>
                            <div className={styles.gallery_lists}>

                                <button>Bed Room</button>


                                <button>Flooring</button>


                                <button>Toilet Fitting</button>


                                <button>Kitchen</button>

                                <button>Vehicle Parking</button>

                            </div>

                            <div className={styles.gallery_section}>
                                <div className={styles.gallery_block}>
                                    <Image src='/images/house8.jpg' height="1000" width="1000" />

                                </div>

                                <div className={styles.gallery_block}>
                                    <div className={styles.gallery_block_two}>
                                        <Image src='/images/house5.jpg' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.gallery_block_three}>
                                        <Image src='/images/house4.jpg' height="1000" width="1000" />
                                    </div>
                                </div>
                                <div className={styles.gallery_block}>
                                    <div className={styles.gallery_block_four}>
                                        <Image src='/images/house7.jpg' height="1000" width="1000" />
                                    </div>
                                    <div className={styles.gallery_block_five}>
                                        <Image src='/images/house6.jpg' height="1000" width="1000" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div>
                    <div className='contain'>
                        <div className={styles.service_section}>
                            {title.map((ind) => {

                                return (
                                    <>
                                        <div className={styles.card_title}>
                                            <p className={styles.small_text}>{ind.small_text}</p>
                                            <h1>{ind.achievetitle}</h1>

                                        </div>
                                        <Slider {...settings}>
                                            {ind.Datas.map((val) => {
                                                return (

                                                    
                                                        <div className={styles.main_cards}>
                                                            <div className={styles.main_cards_container}>
                                                                <div className={styles.cards_block}>
                                                                <div className={styles.icon_sec}>
                                                                    <div className={styles.icon_block}>
                                                                        {val.icon}
                                                                    </div>
                                                                    <p className={styles.text}>{val.description}</p>
                                                                </div>
                                                                <div className={styles.cards_img_block}>
                                                                    <div className={styles.img_block}>
                                                                        <Image src={val.image} height="300" width="300" />
                                                                    </div>
                                                                    <div className={styles.img_names_block}>
                                                                        <h3 className={styles.names}>{val.name}</h3>
                                                                        <p className={styles.light_text}>{val.lighttext}</p>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                  )
                                            })}
                                        
                                        </Slider>
                                    </>
                                )

                            })}
                        </div >

                    </div>
                </div>
                <div className={styles.brands}>
                    <div className='contain'>
                        <h1 className={styles.Trusted_title}>Trusted By Over 100k+ Client</h1>
                        
                        <div className={styles.Trusted_section}>
                        <Slider {...brands}>
                            <div className={styles.Brand_names}>
                                <div className={styles.adobe_blobk}>
                                    <Image src="/images/adobe.png" height="1000" width="1000" />
                                </div>
                            </div>
                            <div className={styles.Brand_names}>
                                <div className={styles.shutter_block}>
                                    <Image src="/images/Shutterstock.png" height="1000" width="1000" />
                                </div>
                            </div>
                            <div className={styles.Brand_names}>
                                <div className={styles.delta_block}>
                                    <Image src="/images/logo1.png" height="1000" width="1000" />
                                </div>
                            </div>
                            <div className={styles.Brand_names}>
                                <div className={styles.kayak_block}>
                                    <Image src="/images/logo2.png" height="1000" width="1000" />
                                </div>
                            </div>
                            <div className={styles.Brand_names}>
                                <div className={styles.lowes_block}>
                                    <Image src="/images/lowes.png" height="1000" width="1000" />
                                </div>
                            </div>
                            
                            </Slider>
                        </div>
                        
                        
                    </div>
                </div>
            </Panellayout>
        </>
    )
}
