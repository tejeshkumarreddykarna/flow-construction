"use client"
import React from 'react'
import styles from './about.module.scss'
import Arrow from '../../../public/icons/arrow-small.svg'
import Image from 'next/image'
import { Panellayout } from '../../../components/panellayout/Panellayout'
import Team from '../../../components/team'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const about = () => {
    const data = [
        {
            img: '/images/image14.png',
            name: 'Mike Simorangkir',
            role: 'Foundor & CEO    ',
            text: 'Mike,MBA, has 1%+ years of experiemce in commercial & mixes use real property development.'
        },
        {
            img: '/images/image13.png',
            name: 'Stanley George',
            role: 'Retired EVP & CCO',
            text: 'George was a partner at Heenan Blakie LLP until September 2005.'
        },
        {
            img: '/images/image17.png',
            name: 'Eliza Baker',
            role: 'Retired EVP & CCO',
            text: 'Eliza,MBA, has 12+ years of experiemce in commercial & mixes use real property development.'
        },
        {
            img: '/images/engineer.jpg',
            name: 'Sally Cruz',
            role: 'Chief Marketing Officer',
            text: 'Terence Cheng, has 10+ years of experiemce in commercial & mixes use real property development.'
        },
        {
            img: '/images/engineer.jpg',
            name: 'Sally Cruz',
            role: 'Chief Marketing Officer',
            text: 'Terence Cheng, has 10+ years of experiemce in commercial & mixes use real property development.'
        },
    ]
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        arrows: false,
        cssEase: "linear",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    
      };
    return (
        <>

            <Panellayout>
                <div className={styles.main_about_container}>
                    <div className='contain'>
                        <p className={styles.title}>We are investors</p>
                        <div className={styles.about_section}>
                            <div className={styles.left_section}>
                                <h1 className={styles.bold_text}>We set out to build<br></br><span>a better way to invest</span></h1>
                                <div className={styles.left_section_img_block}>
                                    <Image src="/images/employe9.jpg" height="2000" width="2000" />
                                    <div className={styles.triangle_bottomright}>

                                    </div>
                                </div>
                            </div>
                            <div className={styles.right_section}>
                                <p className={styles.right_section_block}>Together-the investors and partners <br />of investate-we are reinventing real<br />estate investing end-to-end</p>
                                <div className={styles.right_section_img_block}>
                                    <Image src="/images/employees2.jpg" height="1500" width="1500" />
                                    <div className={styles.triangle_bottomleft}>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={styles.our_story}>
                            <div className={styles.left_section}>
                                <div className={styles.left_bold_txt}>
                                    <h1 className={styles.left_txt_block}>Our Story <Arrow /></h1>
                                </div>
                            </div>
                            <div className={styles.right_section}>
                                <div className={styles.right_block}>
                                    <p> Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.literature, discovered the undoubtable source.</p>
                                </div>
                                <h3 className={styles.left_text}>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</h3>
                                <h4 className={styles.left_texts}>Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words.</h4>
                                <div className={styles.right_img_block}>
                                    <div className={styles.img_block}>
                                        <Image src="/images/image14.png" height="400" width="400" className={styles.girl_img} />
                                        <div className={styles.profile_sec}>
                                            <h2 className={styles.tit}>Mike Simorangkir</h2>
                                            <p className={styles.role}>Founder & CEO</p>
                                        </div>
                                    </div>

                                    <p className={styles.right_last_txt}>"he release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum".</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className={styles.main_conatiner}>
                                <h1 className={styles.bold_text}>Our team has over <span>100 years<br></br> of combined experience</span></h1>
                                <p className={styles.txt}>Our team brings experience, innovation, and creativity to the real estate investing<br></br> experience to ensure your plans are good in nature.</p>

                                <div className={styles.team_cards}>
                                    <Slider {...settings}>
                                        {data.map((item)=>{
                                            return<Team {...{item}}/>
                                        })}
                                    </Slider>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Panellayout>
        </>
    )
}

export default about;