'use client'
import React from 'react'
import { Panellayout } from '../../../components/panellayout/Panellayout'
import Comments from '.././../../public/icons/comments.svg'
import styles from './contact.module.scss'
import Location from '../../../public/icons/mapmarker.svg'
import Phone from '../../../public/icons/phonecall.svg'
import Facebook from '../../../public/icons/Facebook.svg'
import Insta from '../../../public/icons/instagram.svg'
import Youtube from '../../../public/icons/youtube.svg'
import Twitter from '../../../public/icons/twitter.svg'
import Linkin from '../../../public/icons/linkedin.svg'
import { useForm } from 'react-hook-form'
import Input from '../../../components/Input/Input'
const pages = () => {
    const { register, handleSubmit, setValue, formState: { errors } } = useForm()
    const onSubmit = (data) => {
        console.log(data)
    }
    return (
        <Panellayout>
            <div className='contain'>
                <div className={styles.main_contact_sec}>
                    <div className={styles.contact_sec}>
                        <div className={styles.left}>
                            <div className={styles.left_block}>
                                <div className={styles.icon_sec}>
                                    <Comments />
                                </div>
                                <div className={styles.right_block}>
                                    <h3 className={styles.chat}>Chat to us</h3>
                                    <h5 className={styles.help}>Our friendly team is here to help</h5>
                                    <p className={styles.mail}>hi@untitledui.com</p>
                                </div>
                            </div>
                            <div className={styles.left_block}>
                                <div className={styles.icon_sec}>
                                    <Location />
                                </div>
                                <div className={styles.right_block}>
                                    <h3 className={styles.chat}>Vist us</h3>
                                    <h5 className={styles.help}>Come say hello at our office HQ.</h5>
                                    <p className={styles.mail}>100 Smith Street</p>
                                </div>
                            </div>
                            <div className={styles.left_block}>
                                <div className={styles.icon_sec}>
                                    <Phone />
                                </div>
                                <div className={styles.right_block}>
                                    <h3 className={styles.chat}>Call us</h3>
                                    <h5 className={styles.help}>Mon - fri from 8am to 5pm.</h5>
                                    <p className={styles.mail}>+1(555) 000-0000</p>
                                </div>
                            </div>
                        </div>
                        <div className={styles.left_bottom}>
                            <div className={styles.social_media}>

                                <div className={styles.icons}>
                                    <Facebook />
                                </div>
                                <div className={styles.icons}>
                                    <Insta />
                                </div>
                                <div className={styles.icons}>
                                    <Youtube />
                                </div>
                                <div className={styles.icons}>
                                    <Linkin />
                                </div>
                                <div className={styles.icons}>
                                    <Twitter />
                                </div>


                            </div>
                        </div>
                    </div>
                    <div className={styles.right}>
                        <div className={styles.tit_block}>
                            <h1 className={styles.text_bold}>Got ideas? We've got <br />the skills. Let's team up.</h1>
                            <p className={styles.text}>Tell us more about yourself and what you're got in mind.</p>
                        </div>
                        <div className={styles.form_sec}>
                            <form className={styles.form_submit} onSubmit={handleSubmit(onSubmit)}>
                                <Input
                                    {...{
                                        name: 'fullName',
                                        parent: 'contact',
                                        errors,
                                        setValue,
                                        styles,
                                        register,
                                        placeholder: false,
                                        label: false,
                                    }}
                                />
                                <Input
                                    {...{
                                        name: 'email',
                                        parent: 'contact',
                                        errors,
                                        setValue,
                                        styles,
                                        register,
                                        placeholder: false,
                                        label: false,
                                    }}
                                />
                                <Input
                                    {...{
                                        name: 'message',
                                        parent: 'contact',
                                        type: 'textarea',
                                        errors,
                                        setValue,
                                        styles,
                                        register,
                                        placeholder: true,
                                        label: false,

                                    }}
                                />


                                <div className={styles.rioght_sec}>
                                    <p className={styles.text}>How can we help?</p>

                                    <div className={styles.main_chec_box_sec}>


                                        <div className={styles.check_block}>
                                            <label className={styles.container}>Website
                                                <input type="checkbox" checked/>
                                                <span className={styles.checkmark}></span></label>
                                            <label className={styles.container}>Ux design
                                                <input type="checkbox" checked/>
                                                <span className={styles.checkmark}></span></label>
                                            <label className={styles.container}>User Research
                                                <input type="checkbox" />
                                                <span className={styles.checkmark}></span></label>
                                        </div>
                                        <div className={styles.check_block}>
                                            <label className={styles.container}>Content creation
                                                <input type="checkbox" />
                                                <span className={styles.checkmark}></span></label>
                                            <label className={styles.container}>Strategy & consulting
                                                <input type="checkbox" />
                                                <span className={styles.checkmark}></span></label>
                                            <label className={styles.container}>Other
                                                <input type="checkbox" />
                                                <span className={styles.checkmark}></span></label>
                                        </div>
                                    </div>
                                    <div className={styles.btn_block}>
                                        <button type='submit'>Let's get started</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Panellayout>
    )
}

export default pages