import React from 'react'
import styles from './Footer.module.scss'
import Gear from '../../public/icons/gears.svg'
import Insta from '../../public/icons/instagram.svg'
import Facebook from '../../public/icons/facebook.svg'
import Twitter from '../../public/icons/twitter.svg'
import Link from 'next/link'
export const Footer = () => {
    return (
        <div className={styles.footer}>
            <div className='contain'>

                <div className={styles.footer_sec} >
                    <div className={styles.footer_section1}>
                        <p className={styles.icon}><Gear />FlowCon</p>
                        <div>
                            <span className={styles.address}>Pemuda Street Number 10,Bungkal,<br>
                            </br> Ponorogo, East Java, 84578</span>
                            <h3 className={styles.email}>Email: constructo@gmail.com</h3>
                            <h4 className={styles.ph_num}>Phone: 021-99491467</h4>
                        </div>
                    </div>
                    <div className={styles.footer_right}>
                        <div className={styles.main_footer_container}>
                            <div className={styles.footer_section}>
                                <h2 className={styles.title}>Company</h2>
                                <ul className={styles.lists}>
                                    <li><Link href='/' className={styles.link_block}>Home</Link></li>
                                    <li>
                                        <Link href="/about-us" className={styles.link_block}>
                                            About Us
                                        </Link>
                                    </li>
                                    <li>Service</li>
                                    <li>Project</li>
                                </ul>
                            </div>
                            <div className={styles.footer_section}>
                                <h2 className={styles.title}>Information</h2>
                                <ul className={styles.lists}>
                                    <li>Careers</li>
                                    <li><Link href='/contact-us' className={styles.link_block}>Contact Us</Link></li>
                                    <li>Privasi Police</li>
                                    <li>Term & Condition</li>
                                </ul>
                            </div>
                            <div className={styles.footer_section}>
                                <p className={styles.title}>Subscribe To Our Newsletter</p>


                                <div className={styles.email_address}>
                                   <div className={styles.input_block}>
                                    <input type='email' placeholder='Email Address'/>
                                    <div className={styles.btn_block}>
                                        <button>Send</button>

                                    </div>
                                   </div>
                                   
                                </div>


                                <div className={styles.social_media_icons}>
                                    <li><Insta /></li>
                                    <li><Facebook /></li>
                                    <li><Twitter /></li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.main_bottom_section}>
                <div className={styles.bottom_section}>
                    <p>&copy;2022PT.Cronstructo.All right reserved</p>
                </div>
            </div>
        </div>

    )
}
