import React from 'react'
import Image from 'next/image'
import styles from './team.module.scss'
const Team = ({item}) => {
  return (
 
        <div className={styles.team_profile}>
            <div className={styles.team_block}>
            <div className={styles.img_block}>
                <Image src={item.img} height="300" width="300"/>
            </div>
           <div className={styles.details_sec}>
             <h1 className={styles.name}>{item.name}</h1>
             <p className={styles.role}>{item.role}</p>
           </div>
            <div className={styles.text_block}>
                <p className={styles.txt}>{item.text}</p>
            </div>
            </div>
        </div>
   
  )
}

export default Team