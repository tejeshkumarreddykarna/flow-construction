'use client';
import styles from './Header.module.scss'
import Gear from '../../public/icons/gears.svg'
import ArrowRight from '../../public/icons/arrow-small.svg'
import Image from 'next/image'
import Link from 'next/link'
import Menu from '../../public/icons/menu-burger.svg'
import Close from '../../public/icons/circle-xmark.svg'
import React, { useState } from 'react'

export const Header = () => {
    const [toggle, setToggle] = useState(false)
    console.log(toggle)
    return (
        <div className={styles.main_header}>
            <div className='contain'>
                <div className={styles.header_section}>
                    <div className={styles.title}>
                        <p><Gear />FlowCon</p>
                    </div>
                    <ul className={styles.lists}>
                        <li><Link href="/" className={styles.link_block}> Home</Link></li>
                        <li >
                            <Link href="/about-us" className={styles.link_block}>
                                About Us
                            </Link>
                        </li>

                        <li><Link href='/gallery' className={styles.link_block}>Service </Link></li>

                        <li><Link href='/' className={styles.link_block}>Project</Link></li>
                    </ul>

                    <div className={styles.contact_btn}>

                        <button> <Link href="/contact-us" className={styles.link_block}>Contact Us</Link></button>

                    </div>
                    <div className={styles.memu_btn}>
                        <button className={styles.btn} onClick={() => setToggle(!toggle)}><Menu /></button>
                        <div className={`${styles.menu_nav} ${toggle ? styles.menuSection : ''}`}>

                            <div className={styles.menu_section_title}>
                                <div className={styles.title_block}>
                                    <h1 className={styles.title}>Flow-con</h1>
                                </div>
                                <div className={styles.close_icon}>
                                    <button onClick={() => setToggle(!toggle)}><Close /></button>
                                </div>
                                 
                                    
                                            <ul className={styles.lists}>
                                                <li><p></p></li>
                                                <li><Link href="/" className={styles.link_block_mob}>Home</Link></li>

                                                <li><Link href="/about-us" className={styles.link_block_mob}>About Us</Link></li>

                                                <li>Service</li>
                                                <li>Project</li>
                                            </ul>
                                       
                               
                            </div>
                            
                            <div className={styles.contact_btn}>

                                <button> <Link href="/contact-us" className={styles.link_block}>Contact Us</Link></button>

                            </div>
                        </div>
                    </div>


                </div>
                <div className={styles.main_showcase}>
                    <div className={styles.left_section}>
                        <div className={styles.main_text}>
                            <h1 className={styles.left_title}>Create the building you want here </h1>
                            <p className={styles.text}>We provide the best architectural design, construction, and building maintenance services for you.</p>
                        </div>
                        <div className={styles.left_section_btns}>
                            <button>Our Services</button>
                            <p className={styles.text}>View Project <ArrowRight /></p>
                        </div>
                        <div className={styles.mian_left_section_counts}>
                            <div className={styles.left_section_counts}>
                                <h1>10+</h1>
                                <p>Years of Experience</p>
                            </div>
                            <div className={styles.left_section_counts2}>
                                <h1>300</h1>
                                <p>Project Complete</p>
                            </div>
                        </div>
                    </div>
                    <div className={styles.right_section}>
                        <div className={styles.engineer_img}>
                            <div className={styles.img1}>
                                <Image src="/images/engineer.jpg" height="1000" width="1000" />
                            </div>
                            <div className={styles.img2}>
                                <Image src="/images/two_eng.webp" height="1000" width="1000" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

