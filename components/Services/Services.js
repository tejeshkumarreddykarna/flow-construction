import React from 'react'
import Image from 'next/image'
import styles from './services.module.scss'
 const Services = ({value}) => {
    
    return (
        <div className={styles.main_services_section}>
            <div className={styles.service_section_img_block}>
                <div className={styles.img_block}>
                <Image src={value.image} height="300" width="300" />
                </div>
                <div className={styles.service_section_title}>
                    <h2 className={styles.title_block}>{value.title}</h2>
                </div>
                <div className={styles.service_section_description}>
                    <p>{value.description}</p>
                </div>
                <div className={styles.service_section_btn}>
                    <button>{value.button}</button>
                </div>
            </div>
        </div>
    )
}
export default Services;
