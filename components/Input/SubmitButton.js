import React, { useContext } from 'react'
import ButtonLoader from '../ButtonLoader/ButtonLoader'
import { CommonContext } from '../Context/CommonContext';

const SubmitButton = ({ styles, className,type }) => {

    const { Loading } = useContext(CommonContext);
    
    return (
        <button className={`btn ${styles[className] || styles.btn_submit}`} type="submit"  >
            {
                Loading ?
                    <ButtonLoader /> :
                    <span>{type === 'save' ? 'save' :'Submit'}</span>
            }
        </button>
    )
}

export default SubmitButton
