import React from 'react'
import FormFields from '../../Models/FormFields.json'

const Input = ({
    name,
    type = 'text',
    required = null,
    parent,
    register,
    setValue,
    errors,
    styles,
    val,
    label = true,
    inputWrapper = false,
    disabled = false
}) => {

    const Field = parent ? (FormFields[parent])[name] : FormFields[name]
    const InputWrapper = inputWrapper ? 'div' : React.Fragment
    const ErrorClass = errors[name] ? styles.Error : ''

    const InputType = type === 'textarea' ? 'textarea' : 'input'
    const InputFile = type === 'file' ? 'file' : ''

    const RequiredInfo = required ? required : Field.errors.required
  
    return (
        <div className={styles.FormWrapper}>
            {
                label && <label className={styles.FormLabel}>{Field.label}</label>
            }
            <InputWrapper {...(inputWrapper && {className: `${styles.InputWrapper} ${ErrorClass}`})}>
                <InputType 
                    type={type}
                    className={`form-control ${styles.InputField} ${type === 'textarea' && styles.InputFieldLg} ${ErrorClass} ${type === 'file' && styles.inputfile}`}
                    placeholder={Field.placeholder}
                    disabled={disabled}
                    value={val}
                    {
                        ...register(name, {
                            ...(RequiredInfo && {
                                required: RequiredInfo
                            }),
                            ...(Field.errors.pattern && {
                                pattern: {
                                    value: new RegExp(Field.errors.pattern.value),
                                    message: Field.errors.pattern.message
                                }
                            }),
                            ...(Field.errors.maxLength && {
                                maxLength: Field.errors.maxLength
                            }),
                            ...(Field.errors.minLength && {
                                minLength: Field.errors.minLength
                            }),
                        })
                    }
                    onInput={(e) => setValue(name, e.target.value)}
                />
            </InputWrapper>
            {errors[name] && <span className={styles.error_msg}>{errors[name].message}</span>}
        </div>
    )
}

export default Input
