import React from 'react'
import { Header } from '../Header/Header'
import { Footer } from '../Footer/Footer'

export const Panellayout = ({children}) => {
  return (
    <div>
        <Header/>
            {children}
        <Footer/>
    </div>
  )
}
